function calc() {
    var speed = $('#speed');
    var effort = $('#effort');
    var potential = $('#potential');
    var level = $('#level');
    var personality = $('#personality');
    var scarf = $('#scarf');

    speed = parseInt(speed.val());
    effort = parseInt(effort.val());
    potential = parseInt(potential.val());
    level = parseInt(level.val());
    personality = personality.prop('checked');
    scarf = scarf.prop('checked');
    var result = $('#result');

    var ans = (speed*2);
    ans += (effort/4) ;
    ans += potential;
    ans = (ans*(level/100));
    ans = ans+5;

    ans = Math.floor(ans);
    if (personality) {
        ans = ans * 1.1;
    }
    if (scarf) {
        ans = ans * 1.5;
    }

    ans = Math.floor(ans);
    result.text(ans);
}

$(function(){
    $('.input').focus(function(){
        $(this).select();
    });
    $('input[type!=hidden]:first').focus();
    $('.btn').on('click',function(){
        calc();
        return;
    });
    $( '.input' ).keypress( function ( e ) {
        if ( e.which == 13 ) {
            calc();
            return false;
        }
    } );
    $( '.check' ).keypress( function ( e ) {
        if ( e.which == 13 ) {
            calc();
            return false;
        }
    } );
});

